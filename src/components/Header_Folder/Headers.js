import { Button, Card } from 'react-bootstrap';
import Badge from 'react-bootstrap/Badge';
import { NavLink } from 'react-router-dom';
import './Headers.css'



export default function Header() {
  return (
  <div className='mainContainerHeader'>
    <Card className="bg-dark text-white">
      <Card.Img src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/7307e562514417.5a931ab904cad.gif" alt="Card image" />
      <Card.ImgOverlay>
        <Card.Title>
          <Badge as={NavLink} to="/products" className='badgeHeader' bg="danger">HOT!</Badge>
        </Card.Title>
      </Card.ImgOverlay>
    </Card>
  </div>
  );
}