import { Send } from "@material-ui/icons";
import styled from "styled-components";
import { mobile } from "../responsive";
import Swal from "sweetalert2";
import { useState } from "react";
import { green } from "@material-ui/core/colors";

const Container = styled.div`
  padding: 10%;
  margin-top: 10px;
  height: 60vh;
  background-color: #fcf5f5;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background-image: url("https://images.unsplash.com/photo-1499750310107-5fef28a66643?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80");
  background-repeat: no-repeat;
  background-size: 100%;
  height: 60%;
  ${mobile({ padding: "5%" })}
`;


const Title = styled.h1`
  font-size: 70px;
  margin-bottom: 20px;
  color: white;
  ${mobile({ fontSize: "30px", textAlign: "center" })}
`;

const Desc = styled.div`
  font-size: 24px;
  font-color: 
  font-weight: 300;
  margin-bottom: 20px;
  color: white;
  ${mobile({ textAlign: "center", fontSize: "16px" })}
`;


const InputContainer = styled.div`
  width: 50%;
  height: 60px;
  background-color: white;
  display: flex;
  justify-content: space-between;
  border: 1px solid lightgray;
  padding: 10px;
  ${mobile({ width: "80%", padding: "5px", height: "30px" })}
`;


const Input = styled.input`
  border: none;
  flex: 8;
  padding-left: 20px;
  ${mobile({ fontSize: "14px" })}
`;


const Button = styled.button`
  flex: 1;
  border: none;
  background-color: teal;
  color: white;
`;

const justSend = () => {
    Swal.fire({
        title: "Subscribed!",
        icon: "success",
        text: "Happy Shopping!"
    });
}

const Newsletter = () => {
    const [email, setEmail] = useState('');

    const justSend = () => {
        Swal.fire({
          title: 'Subscribed!',
          icon: 'success',
          text: 'Email has been sent successfully!',
        });
        setEmail('');
      };

  return (
    <Container>
      <Title>Need Updates? Subscribe!</Title>
      <Desc>Get timely updates from your favorite products.</Desc>
      <InputContainer>
        <Input placeholder="Your email" value={email} onChange={(e) => setEmail(e.target.value)} />
        <Button onClick={() => justSend()}>
          <Send />
        </Button>
      </InputContainer>
    </Container>
  );
};

export default Newsletter;


