import {Container, Navbar, Nav} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom';
import {useState, Fragment, useContext} from 'react'; //
import UserContext from '../../UserContext';
import Logo from './1.png';

// CSS
import './AppNavbar.css'

// for NavBar
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Offcanvas from 'react-bootstrap/Offcanvas';


//Material UI
import Badge from '@mui/material/Badge';
import { styled } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart'

// for Materials Badge
const StyledBadge = styled(Badge)(({ theme }) => ({
	'& .MuiBadge-badge': {
	  right: -3,
	  top: 13,
	  border: `2px solid ${theme.palette.background.paper}`,
	  padding: '0 4px',
	},
  }));

export default function AppNavbar(){
	
	// const [user, setUser] = useState(localStorage.getItem("email"));
	const { user, setUser } = useContext(UserContext);
	const [showNav, setShowNav] = useState(true);
	console.log(user);

	const [userId, setUserId] = useState(localStorage.getItem("userId"));

	const handleNavClose = () => setShowNav(false);

	

	return(
		<div className='navBarContainer'>
		{['xl'].map((expand) => (
		  <Navbar key={expand} bg="light" expand={expand} className="mb-3 mainAppNavBar">
			<Container fluid>
				<img className='logoNav' src={Logo} alt='logo image'/>
			  <Navbar.Brand className='titleNav' as={Link} to="/">SENDA SHOES COLLECTION</Navbar.Brand>
			  <IconButton aria-label="cart" as={NavLink} to="/cart" onClick={handleNavClose}>
  						<StyledBadge badgeContent={0} color="secondary">
    						<ShoppingCartIcon />
  						</StyledBadge>
					</IconButton>
			  <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
			  <Navbar.Offcanvas
				id={`offcanvasNavbar-expand-${expand}`}
				aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
				placement="end" style={{ maxHeight: 'none' }}>
				<Offcanvas.Body>
				  <Nav className="justify-content-end flex-grow-1 pe-3">
					<Nav.Link as={NavLink} to="/" onClick={handleNavClose}>HOME</Nav.Link>
					<Nav.Link as={NavLink} to="/products" onClick={handleNavClose}>ALL PRODUCTS</Nav.Link>

					{(user.id !== null && userId === null)?

					<Nav.Link as={NavLink} to="/logout" onClick={handleNavClose}>LOGOUT</Nav.Link>
					: 
					<Nav.Link as={NavLink} to="/login" onClick={handleNavClose}>LOGIN</Nav.Link>
					}
					<Nav.Link as={NavLink} to="/contact" onClick={handleNavClose}>CONTACT</Nav.Link>
				  </Nav>
				  <Form className="d-flex">
					<Form.Control
					  type="search"
					  placeholder="Search"
					  className="me-2"
					  aria-label="Search"
					/>
					<Button variant="outline-success">Search</Button>
				  </Form>
				</Offcanvas.Body>
			  </Navbar.Offcanvas>
			</Container>
		  </Navbar>
		))}
	  </div>
	)
}
