import { useContext, useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { Container, Row, Col, Card, Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";
import UserContext from "../../UserContext";

export default function ProductView() {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const { productId } = useParams();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [productStock, setProductStock] = useState("");
  const [quantity, setQuantity] = useState("");
  const [userId, setUserId] = useState(localStorage.getItem("userId"))

  useEffect(() => {
    console.log(productId);

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setProductStock(data.stocks);
      });
  }, [productId]);

  const addCart = () => {
    if (productStock < quantity) {
      Swal.fire({
        title: "Insufficient stock.",
        icon: "error",
        text: "Please select a lower quantity.",
      });
    } else {
      fetch(`${process.env.REACT_APP_API_URL}/orders/add`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          productId: productId,
          quantity: quantity,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log("Add to Cart Data");
          console.log(data);

          if (data) {
            Swal.fire({
              title: "Added to Cart!",
              icon: "success",
              text: "Thank you for Shopping!",
            });
            navigate("/products");
          } else {
            Swal.fire({
              title: "Something went wrong.",
              icon: "error",
              text: "Please try again.",
            });
          }
        });
    }
  };

  return (
    <Container className="mt-5 productViewContainer">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <Form.Control
                type="number" value={quantity} onChange={(event) => setQuantity(parseInt(event.target.value))}
              />
              <p>Stock: {productStock-quantity}</p>
              {(user.id !== null || userId === null) ? (
                <Button variant="primary" size="lg" onClick={addCart}>Add to Cart</Button>
              ) : (
                <Button as={Link} to="/login" variant="success" size="lg">Login to Buy</Button>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}
