import { SettingsSystemDaydreamOutlined } from '@material-ui/icons';
import { Fragment } from 'react';
import { Button } from 'react-bootstrap';
import FloatingLabel from 'react-bootstrap/FloatingLabel';
import Form from 'react-bootstrap/Form';
import Footer from '../../components/Footer_Folder/Footer';
import './Contact.css';
import { useState } from 'react';
import Swal from 'sweetalert2';


export default function Contact() {

    const [email, setEmail] = useState('');
    const [comment, setComment] = useState('');

    const justSend = () => {
        Swal.fire({
          title: 'Thanks for the Feedback!',
          icon: 'success',
          text: 'Happy Shopping!',
        });
        setEmail('');
        setComment('');
      };


  return (
    
    <Fragment>
        
        <div className='mainContainerContact'>
            <div className='row'>
            {/* Left column for the form */}
            <div className='col-md-6'>
                <h1>Have a Question? Contact us!</h1>
                <div className='textAreaContact'>
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>

                    <FloatingLabel controlId="floatingTextarea2" label="Comments" value={comment} onChange={(e) => setComment(e.target.value)}>
                        <Form.Control
                        as="textarea"
                        placeholder="Leave a comment here"
                        style={{ height: '100px' }}
                        />
                    </FloatingLabel>
                    </Form.Group>
                    <Button variant="primary" type="button" onClick={justSend}>Send a Message</Button>
                </Form>
                </div>
            </div>

            {/* Right column for the picture */}
            <div className='col-md-6'>
                <div>
                <img className='pcitureContact' src='https://media.istockphoto.com/id/1311934969/photo/contact-us.jpg?s=612x612&w=0&k=20&c=_vmYyAX0aFi-sHH8eYS-tLLNfs1ZWXnNB8M7_KWwhgg=' alt="Contact Image" />
                </div>
            </div>
            </div>
        </div>


        {/* // Additional: */}
        <Footer/>
    </Fragment>
  );
}